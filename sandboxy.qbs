import qbs 1.0

Project {
    qbsSearchPaths: "qbs"

    property string base_path: "bin"
    property string library_path: base_path
    property string config_path: base_path.concat("/config")

    property stringList defines: [ /*"ELPP_QT_LOGGING" since qt NEENOOZHEEN*/ ]

    property string tests_enabled: false

    references: [
        "src/src.qbs",
        "configs/configs.qbs",
        "assets/assets.qbs"
    ]
}
