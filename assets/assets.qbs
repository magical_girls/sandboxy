import qbs 1.0

Product {
    name: "assets"

    property string install_dir: project.base_path.concat("/assets")

    Group {
        name: "Fonts"
        files: "fonts/*"

        qbs.install: true
        qbs.installDir: install_dir.concat("/fonts")
    }
}
