import qbs 1.0

Product {
    name: "configs"

    property string install_dir: project.config_path

    Group {
        name: "Config Resource"
        files: "*.conf"

        qbs.install: true
        qbs.installDir: install_dir
    }
}
