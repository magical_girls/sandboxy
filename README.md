# README #

Sandboxy - first CAD system with graph-based modeling system on Parasolid core.

# Requirements #
- Boost 1.58+
- glm
- glfm
- Parasolid v26.0+

# Building #
## On windows: ##
- Get all requiremented libraries. Set env variables `BOOST_HOME`, `GLM_HOME`, `GLFM_HOME`.
## On linux: ##
No additional steps.
Then on both systems: 
-`git clone git@bitbucket.org:magical_girls/sandboxcad.git`
-`cd sandboxcad`
-`qbs build` or `qbs run --products SandboxCAD`
