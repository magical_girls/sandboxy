#include <logger/logger.h>
#include <parasolid/session.h>
#include <parasolid/builder.h>
#include <gui/application.h>

SHARE_EASYLOGGINGPP(getEasyloggingStorage())

using namespace std;

auto main(int argc, char *argv[]) -> int {
    START_EASYLOGGINGPP(argc, argv);

    parasolid::Session session;
    session.initialize();

    gui::Application app(session.getRenderer(), session.getBuilder());
    app.init();

    app.runEventLoop();
    return 0;
}
