import qbs 1.0
import SandboxyProduct

SandboxyProduct {
    name: "sandboxy"
    type: "application"
    install_dir: project.base_path

    files: [
        "main.cpp"
    ]

    Depends { name: "api" }
    Depends { name: "logger" }

    Depends { name: "parasolid" }
    Depends { name: "gui" }

    property stringList main_libs: ["boost_system"]
    property string glm_home: qbs.getEnv("GLM_HOME")

    Properties {
        condition: qbs.targetOS.contains("windows")

        property string boost_home: qbs.getEnv("BOOST_HOME")

        cpp.includePaths: [
            glm_home + "/include",
            boost_home + "/include"
        ]

        cpp.libraryPaths: [
            boost_home + "/lib"
        ]

        cpp.dynamicLibraries: main_libs
    }

    Properties {
        condition: qbs.targetOS.contains("linux")

        cpp.includePaths: [
            glm_home + "/include"
        ]
        cpp.dynamicLibraries: main_libs
    }
}

