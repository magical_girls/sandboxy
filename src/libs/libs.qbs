import qbs 1.0

Project {
    name: "libraries"

    references: [
        "api/api.qbs",
        "imgui/imgui.qbs",
        "logger/logger.qbs",
        "glew/glew.qbs"
    ]
}
