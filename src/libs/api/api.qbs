import qbs 1.0
import SandboxyLibrary

SandboxyLibrary {
    name: "api"

    files: [
        "builder/node.h",
        "builder/node_slot.cpp",
        "builder/node_slot.h",
        "ibuilder.h",
        "irenderer.h",
    ]

    property stringList api_libs: ["boost_system"]
    property string glm_home: qbs.getEnv("GLM_HOME")

    Properties {
        condition: qbs.targetOS.contains("windows")

        property string boost_home: qbs.getEnv("BOOST_HOME")

        cpp.includePaths: [
            glm_home + "/include",
            boost_home + "/include"
        ]

        cpp.libraryPaths: [
            boost_home + "/lib"
        ]

        cpp.dynamicLibraries: api_libs
    }

    Properties {
        condition: qbs.targetOS.contains("linux")

        cpp.includePaths: [
            glm_home + "/include"
        ]
        cpp.dynamicLibraries: api_libs
    }


}
