#ifndef NODE_SLOT_H
#define NODE_SLOT_H

#include <memory>
#include <boost/uuid/uuid.hpp>

namespace api {


class node_slot {
public:
    enum SLOT_TYPE {
        INPUT, OUTPUT
    };

    node_slot(boost::uuids::uuid id, SLOT_TYPE type);
    ~node_slot();
    node_slot(const node_slot &other) = default;
    node_slot& operator=(const node_slot &other) = default;

    boost::uuids::uuid id() const;
    SLOT_TYPE type() const;

private:
    boost::uuids::uuid m_id;
    SLOT_TYPE m_type;
};

inline bool operator ==(node_slot const& lhs, node_slot const& rhs);

} // namespace api

#endif // NODE_SLOT_H
