#ifndef NODE_H
#define NODE_H

#include <memory>
#include <vector>
#include "node_slot.h"

namespace api {

class node_base {
public:
    node_base();
    node_base(const node_base& node);
    virtual ~node_base();

    //get unique node id
    boost::uuids::uuid getId() const;
    //get node name prefix
    std::string getNodePrefix() const;
    //get node's input slots
    std::vector<node_slot> &getInputSlots();
    //get node's output slots
    std::vector<node_slot> &getOutputSlots();

protected:
    class data;

    data& get_data_reference();
    data const& get_data_const_reference() const;

//private:
    std::shared_ptr<data> m_data;
};

class object_node : public node_base {
public:
    object_node(boost::uuids::uuid uuid);
    virtual ~object_node();

//    std::string getNodePrefix() const override;
//    std::vector<node_slot> getInputSlots() override;
//    std::vector<node_slot> getOutputSlots() override;

protected:
    class data;

private:
    std::shared_ptr<data> m_data;
};

} // namespace api

#endif // NODE_H
