#include "node_slot.h"
#include <sstream>

using namespace api;

node_slot::node_slot(boost::uuids::uuid id, api::node_slot::SLOT_TYPE type)
    : m_id(id), m_type(type)
{

}

node_slot::~node_slot()
{

}

boost::uuids::uuid node_slot::id() const
{
    return m_id;
}

node_slot::SLOT_TYPE node_slot::type() const
{
    return m_type;
}

bool operator ==(const node_slot &lhs, const node_slot &rhs)
{
    return lhs.id() == rhs.id();
}
