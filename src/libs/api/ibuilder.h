#ifndef IBUILDER_H
#define IBUILDER_H

#include "builder/node.h"

namespace api {

///
/// \brief Интерфейс класса-построителя
///
/// Используя данный интерфейс gui модуль будет взаимодействовать с
/// функциями построения модели.
/// Должен реализовываться классом внутири core-модуля.
/// Указатель на объект, реализующий данный интерфейс передается
/// в класс gui в момент создания.
///
class IBuilder
{
public:
    virtual void buildDemo() = 0;
    virtual node_base createObjectNode() = 0;
};
} // namespace api

#endif // IBUILDER_H
