#ifndef IRENDERER_H
#define IRENDERER_H

#include <glm/glm.hpp>
#include <vector>

namespace api {

struct model {
    std::vector<glm::vec3> coords;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec3> colours;
    std::vector<glm::vec3> indexes;
    std::vector<std::vector<glm::vec3>> wires;
};

class IRenderer
{
public:
    virtual std::vector<model> getModelsData() = 0;
};
} // namespace api

#endif // IRENDERER_H
