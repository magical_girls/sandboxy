#ifndef LOGGER_H
#define LOGGER_H

#include "easylogging++.h"

el::base::type::StoragePointer getEasyloggingStorage();

#endif // LOGGER_H

