import qbs 1.0
import SandboxyLibrary

SandboxyLibrary {
    name: "logger"

    files: [
        "easylogging++.h",
        "logger.cpp",
        "logger.h",
    ]
}
