#include "logger.h"

INITIALIZE_EASYLOGGINGPP

int initLogging() {
    el::Configurations c;
    c.setGlobally(el::ConfigurationType::Format, "[%datetime{%H:%m:%s:%g}][%level][%func] %msg");
    el::Loggers::reconfigureLogger("default", c);
    el::Loggers::setDefaultConfigurations(c, true);
    return 0;
}

el::base::type::StoragePointer getEasyloggingStorage() {
    return el::Helpers::storage();
}

static int code = initLogging();
