import qbs 1.0
import SandboxyLibrary

SandboxyLibrary {
    files: [
        "GL/glew.h",
        "GL/glxew.h",
        "GL/wglew.h",
        "src/glew.c",
    ]

    cpp.defines: [ "GLEW_STATIC" ]

    property string freeglut_home: qbs.getEnv("FREEGLUT_HOME")

    Properties {
        condition: qbs.targetOS.contains("windows")

        cpp.includePaths: [
            freeglut_home + "/include"
        ]
        cpp.libraryPaths: [
            freeglut_home + "/lib"
        ]

        cpp.dynamicLibraries: [
            "freeglut",
            "opengl32",
            "imm32"
        ]
    }

    Properties {
        condition: qbs.targetOS.contains("linux")

        cpp.dynamicLibraries: [
            "glut",
            "GL"
        ]
    }

}
