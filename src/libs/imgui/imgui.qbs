import qbs 1.0
import SandboxyLibrary

SandboxyLibrary {
    name: "imgui"

    files: [
        "imconfig.h",
        "imgui.cpp",
        "imgui.h",
        "imgui_demo.cpp",
        "imgui_draw.cpp",
        "imgui_internal.h",
        "stb_rect_pack.h",
        "stb_textedit.h",
        "stb_truetype.h",
    ]

    // Нужны ли вообще ему все эти либы? Под пинусом все собирается без линковки
/*
    property string freeglut_home: qbs.getEnv("FREEGLUT_HOME")
    property string glfw_home: qbs.getEnv("GLFW_HOME")
*/

    Properties {
        condition: qbs.targetOS.contains("windows")

//        cpp.includePaths: [
//            freeglut_home + "/include",
//            glfw_home + "/include"
//        ]

//        cpp.libraryPaths: [
//            freeglut_home + "/lib",
//            glfw_home + "/lib"
//        ]

        cpp.dynamicLibraries: [
//            "freeglut",
//            "glfw3",
            "imm32"
        ]
    }
/*
    Properties {
        condition: qbs.targetOS.contains("linux")

        cpp.dynamicLibraries: [
            "glut3",
            "glfw"
        ]
    }*/
}
