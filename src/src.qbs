import qbs 1.0

Project {
    name: "sources"

    references: [
        "application/application.qbs",
        "libs/libs.qbs",
        "modules/modules.qbs"
    ]
}
