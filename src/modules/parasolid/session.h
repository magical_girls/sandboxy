#ifndef SESSION_H
#define SESSION_H

#include "renderer.h"
#include "builder.h"

namespace parasolid {
namespace frustrum {
class Initializer;
}

class Session
{
public:
    Session();
    ~Session();

    void initialize();

    std::shared_ptr<Renderer> getRenderer() const;
    std::shared_ptr<Builder> getBuilder() const;

private:
    frustrum::Initializer *frustrumInitializer;
    static bool m_initialized;

    std::shared_ptr<Renderer> renderer;
    std::shared_ptr<Builder> builder;
};

}

#endif // SESSION_H
