#include <logger/logger.h>

#include <parasolid_kernel.h>

#include "frustrum/frustruminitializer.h"
#include "session.h"

using namespace parasolid;

bool Session::m_initialized = false;

Session::Session() :
    frustrumInitializer(new frustrum::Initializer(frustrum::Holder()))
{
    frustrumInitializer->initHolder();

    renderer = std::make_shared<Renderer>();
    builder = std::make_shared<Builder>();
}

Session::~Session()
{
    delete frustrumInitializer;
}

void Session::initialize()
{
    frustrumInitializer->registerFrustrum();
    PK_SESSION_start_o_t options;
    PK_SESSION_start_o_m(options);

    if (PK_SESSION_start(&options) == PK_ERROR_no_errors) {
        LOG(INFO) << "Session started fine";
        m_initialized = true;
    } else {
        LOG(FATAL) << "Session started fault";
        m_initialized = false;
    }
}

std::shared_ptr<Renderer> Session::getRenderer() const
{
    return renderer;
}

std::shared_ptr<Builder> Session::getBuilder() const
{
    return builder;
}
