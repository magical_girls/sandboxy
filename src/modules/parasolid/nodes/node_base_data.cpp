#include "node_base_data.h"

namespace api {

node_base::data::data(boost::uuids::uuid uuid) :
    m_uuid(uuid)
{
}

boost::uuids::uuid node_base::data::getId()
{
    return m_uuid;
}

std::string node_base::data::getNodePrefix()
{
    return "base_";
}

}
