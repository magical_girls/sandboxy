#include "object_node_data.h"

namespace api {

object_node::data::data(boost::uuids::uuid id) : node_base::data(id)
{
}

std::string object_node::data::getNodePrefix()
{
    return "obj_";
}

std::vector<api::node_slot> &object_node::data::getInputSlots()
{
    return m_input_slots;
}

std::vector<api::node_slot> &object_node::data::getOutputSlots()
{
    return m_output_slots;
}
}
