#include <api/builder/node.h>
#include "object_node_data.h"

using namespace boost;

namespace api {



object_node::object_node(uuids::uuid id)
//    : node_base(id)
{
    m_data = std::shared_ptr<object_node::data>(new object_node::data(id));
    node_base::m_data = m_data;
}

object_node::~object_node()
{

}

//std::string object_node::getNodePrefix() const
//{
//    return m_data->getNodePrefix();
//}

//std::vector<api::node_slot> object_node::getInputSlots()
//{
//    return m_data->getInputSlots();
//}

//std::vector<api::node_slot> object_node::getOutputSlots()
//{
//    return m_data->getOutputSlots();
//}

} // namespace api
