#ifndef NODE_BASE_DATA_H
#define NODE_BASE_DATA_H

#include <api/builder/node.h>

namespace api {

class node_base::data
{
public:
    data(boost::uuids::uuid uuid);
    boost::uuids::uuid getId();
    virtual std::string getNodePrefix();

    virtual std::vector<api::node_slot> &getInputSlots() = 0;
    virtual std::vector<api::node_slot> &getOutputSlots() = 0;

private:
    boost::uuids::uuid m_uuid;
};
} // namespace api

#endif // NODE_BASE_DATA_H
