#ifndef OBJECT_NODE_DATA_H
#define OBJECT_NODE_DATA_H

#include "node_base_data.h"

namespace api {


class object_node::data : public node_base::data
{
public:
    data(boost::uuids::uuid id);

    virtual std::string getNodePrefix() override;
    virtual std::vector<api::node_slot> &getInputSlots();
    virtual std::vector<api::node_slot> &getOutputSlots();

private:
    std::vector<api::node_slot> m_input_slots;
    std::vector<api::node_slot> m_output_slots;
};
} // namespace api

#endif // OBJECT_NODE_DATA_H
