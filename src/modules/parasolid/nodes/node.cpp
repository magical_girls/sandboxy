#include <api/builder/node.h>
#include "node_base_data.h"

namespace api {

node_base::node_base()
{
    //    m_data = std::shared_ptr<data>(new data(uuid));
}

node_base::node_base(const node_base &node)
{
    m_data = node.m_data;
}

node_base::~node_base()
{
}

boost::uuids::uuid node_base::getId() const
{
    return m_data->getId();
}

std::string node_base::getNodePrefix() const
{
    return m_data->getNodePrefix();
}


std::vector<api::node_slot>& node_base::getInputSlots()
{
    return m_data->getInputSlots();
}

std::vector<api::node_slot>& node_base::getOutputSlots()
{
    return m_data->getOutputSlots();
}

node_base::data &node_base::get_data_reference()
{
    return *m_data.operator->();
}

const node_base::data &node_base::get_data_const_reference() const
{
    return *m_data.operator->();
}

} // namespace api
