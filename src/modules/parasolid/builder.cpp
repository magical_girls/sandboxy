#include "builder.h"
#include "demo.h"
#include <api/builder/node.h>
#include <api/builder/node_slot.h>
#include <boost/uuid/uuid_io.hpp>
#include <logger/logger.h>

using namespace parasolid;
using namespace api;
using namespace boost;
using namespace std;

Builder::Builder()
{

}


void Builder::buildDemo()
{
    Demo::buildCube();
}

node_base Builder::createObjectNode()
{
    boost::uuids::uuid id = m_uuids_generator();
    LOG(TRACE) << "Creating object node id(" << uuids::to_string(id) << ")";
    object_node node = object_node(id);
    node.getInputSlots().emplace_back(m_uuids_generator(), api::node_slot::INPUT);
    node.getOutputSlots().emplace_back(m_uuids_generator(), api::node_slot::OUTPUT);
    return node;
}
