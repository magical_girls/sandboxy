import qbs 1.0
import SandboxyModule

SandboxyModule {
    name: "parasolid"

    files: [
        "frustrum/frustrumdelta.cpp",
        "frustrum/frustrumerror.cpp",
        "frustrum/frustrumfunctions.h",
        "frustrum/frustrumholder.cpp",
        "frustrum/frustrumholder.h",
        "frustrum/frustruminitializer.cpp",
        "frustrum/frustruminitializer.h",
        "frustrum/gofrustrum.cpp",
        "frustrum/mainfrustrum.cpp",
        "builder.cpp",
        "builder.h",
        "nodes/node.cpp",
        "nodes/node_base_data.cpp",
        "nodes/node_base_data.h",
        "nodes/object_node_data.cpp",
        "nodes/object_node_data.h",
        "nodes/object_node.cpp",
        "renderer.cpp",
        "renderer.h",
        "session.cpp",
        "session.h",
        "demo.cpp",
        "demo.h",
    ]

    property stringList gui_libs: ["boost_system", "boost_filesystem", "pskernel"]

    property string parasolid_home: qbs.getEnv("PARASOLID_HOME")
    property string glm_home: qbs.getEnv("GLM_HOME")
    property string boost_home: qbs.getEnv("BOOST_HOME")

    cpp.includePaths: [
        parasolid_home + "/include",
        boost_home + "/include",
        glm_home + "/include"
    ]

    cpp.libraryPaths: [
        parasolid_home + "/lib",
        boost_home + "/lib"
    ]

    cpp.dynamicLibraries: gui_libs

    Depends { name: "api" }
}
