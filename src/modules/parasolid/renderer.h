#ifndef RENDERER_H
#define RENDERER_H

#include <api/irenderer.h>

namespace parasolid {

class Renderer : public api::IRenderer
{
public:
    Renderer();
    virtual std::vector<api::model> getModelsData() override;
};
} // namespace parasolid

#endif // RENDERER_H
