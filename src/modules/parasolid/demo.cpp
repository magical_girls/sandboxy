#include <logger/logger.h>

#include <parasolid_kernel.h>

#include "demo.h"

Demo::Demo()
{

}

void Demo::buildCube()
{
    PK_BODY_t body;
    PK_AXIS2_sf_s ax;
    ax.axis = { 1, 0, 0 };
    ax.ref_direction = { 0, 1, 0 };
    ax.location = { 0, 0, 0 };
    PK_BODY_create_solid_block(5, 5, 5, &ax, &body);
    LOG(INFO) << "Created cube " << body;
}

