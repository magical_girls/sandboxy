#ifndef BUILDER_H
#define BUILDER_H

#include <api/ibuilder.h>
#include <boost/uuid/uuid_generators.hpp>

namespace parasolid {

class Builder : public api::IBuilder
{
public:
    Builder();
    void buildDemo() override;

    api::node_base createObjectNode() override;

private:
    boost::uuids::random_generator m_uuids_generator;
};
} // namespace parasolid

#endif // BUILDER_H
