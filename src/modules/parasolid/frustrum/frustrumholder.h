#ifndef FRUSTRUMHOLDER_H
#define FRUSTRUMHOLDER_H

#include <parasolid_kernel.h>

namespace parasolid {
namespace frustrum {


class Holder
{
public:
    Holder();
    ~Holder();

    struct MainFrustrum {
        void (*startFrustrum)(int *);
        void (*stopFrustrum)(int *);
        void (*getMemory)(int *, char **, int *);
        void (*returnMemory)(int *, char **, int *);
        void (*startFileFrustrum)(int *);
        void (*abortFrustrum)(int *);
        void (*stopFileFrustrum)(int *);
        void (*openReadFrustrumFile)(const int *, const int *, const char *, const int *, const int *, int *, int *);
        void (*openWriteFrustrumFile)(const int *, const int *, const char *,const int *, const char *, const int *, int *, int *);
        void (*readFromFrustrumFile)(const int *, const int *, const int *, char *, int *, int *);
        void (*writeToFrustrumFile)(const int *, const int *, const int *, const char *, int *);
        void (*closeFrustrumFile)(const int *, const int *, const int *, int *);
    };

    struct DeltaFrustrum {
        PK_ERROR_code_t (*openForWrite)(PK_PMARK_t, PK_DELTA_t *);
        PK_ERROR_code_t (*openForRead)(PK_DELTA_t);
        PK_ERROR_code_t (*write)(PK_DELTA_t, unsigned, const char *);
        PK_ERROR_code_t (*read)(PK_DELTA_t, unsigned, char *);
        PK_ERROR_code_t (*deletef)(PK_DELTA_t);
        PK_ERROR_code_t (*close)(PK_DELTA_t);
        PK_ERROR_code_t (*init)(int);
    };

    struct ErrorFrustrum {
        PK_ERROR_code_t (*errorHandler)(PK_ERROR_sf_t *);
    };

    struct GoFrustrum {
        void (*openSegment)(const int *, const int *, const int *, const int *, const double *, const int *, const int *, int *);
        void (*closeSegment)(const int *, const int *, const int *, const int *, const double *, const int *, const int *, int *);
        void (*outputSegment)(const int *, const int *, const int *, const int *, const double *, const int *, const int *, int *);
    };

    MainFrustrum mainF() const;
    void setMainF(const MainFrustrum &mainF);

    DeltaFrustrum deltaF() const;
    void setDeltaF(const DeltaFrustrum &deltaF);

    ErrorFrustrum errorF() const;
    void setErrorF(const ErrorFrustrum &errorF);

    GoFrustrum goF() const;
    void setGoF(const GoFrustrum &goF);

private:
    MainFrustrum m_mainF;
    DeltaFrustrum m_deltaF;
    ErrorFrustrum m_errorF;
    GoFrustrum m_goF;
};
}}

#endif // FRUSTRUMHOLDER_H
