#include <logger/logger.h>

#include "frustruminitializer.h"
#include "frustrumfunctions.h"

using namespace parasolid::frustrum;

Initializer::Initializer(Holder holder) :
    m_holder(std::move(holder))
{

}

void Initializer::initHolder()
{
    Holder::MainFrustrum main;
    Holder::DeltaFrustrum delta;
    Holder::ErrorFrustrum error;
    Holder::GoFrustrum go;

    main.startFrustrum = startFrustrum;
    main.abortFrustrum = abortFrustrum;
    main.stopFrustrum = stopFrustrum;
    main.getMemory = getMemory;
    main.returnMemory = returnMemory;
    main.openReadFrustrumFile = openReadFrustrumFile;
    main.openWriteFrustrumFile = openWriteFrustrumFile;
    main.closeFrustrumFile = closeFrustrumFile;
    main.readFromFrustrumFile = readFromFrustrumFile;
    main.writeToFrustrumFile = writeToFrustrumFile;

    delta.openForWrite = FRU_delta_open_for_write;
    delta.openForRead = FRU_delta_open_for_read;
    delta.close = FRU_delta_close;
    delta.write = FRU_delta_write;
    delta.read = FRU_delta_read;
    delta.deletef = FRU_delta_delete;

    error.errorHandler = errorHandler;

    go.openSegment = openSegment;
    go.closeSegment = closeSegment;
    go.outputSegment = outputSegment;

    m_holder.setMainF(main);
    m_holder.setDeltaF(delta);
    m_holder.setGoF(go);
    m_holder.setErrorF(error);
}

bool Initializer::registerFrustrum()
{
    PK_SESSION_frustrum_t fru;
    PK_SESSION_frustrum_o_m(fru);

    // main frustrum
    fru.fstart = m_holder.mainF().startFrustrum;
    fru.fabort = m_holder.mainF().abortFrustrum;
    fru.fstop = m_holder.mainF().stopFrustrum;
    fru.fmallo = m_holder.mainF().getMemory;
    fru.fmfree = m_holder.mainF().returnMemory;
    fru.ffoprd = m_holder.mainF().openReadFrustrumFile;
    fru.ffopwr = m_holder.mainF().openWriteFrustrumFile;
    fru.ffclos = m_holder.mainF().closeFrustrumFile;
    fru.ffread = m_holder.mainF().readFromFrustrumFile;
    fru.ffwrit = m_holder.mainF().writeToFrustrumFile;

    fru.gosgmt = m_holder.goF().outputSegment;
    fru.goopsg = m_holder.goF().openSegment;
    fru.goclsg = m_holder.goF().closeSegment;

    PK_SESSION_register_frustrum(&fru);
    LOG(TRACE) << "Frustrum function registred";


    PK_DELTA_frustrum_t delta_fru;
    delta_fru.open_for_write_fn = m_holder.deltaF().openForWrite;
    delta_fru.open_for_read_fn = m_holder.deltaF().openForRead;
    delta_fru.close_fn = m_holder.deltaF().close;
    delta_fru.write_fn = m_holder.deltaF().write;
    delta_fru.read_fn = m_holder.deltaF().read;
    delta_fru.delete_fn = m_holder.deltaF().deletef;

    if (PK_DELTA_register_callbacks(delta_fru) != PK_ERROR_no_errors) {
        return false;
    }
    LOG(TRACE) << "Delta Frustrum function registred";

    PK_ERROR_frustrum_t errorFru;
    errorFru.handler_fn = m_holder.errorF().errorHandler;
    if (PK_ERROR_register_callbacks(errorFru) != PK_ERROR_no_errors) {
        return false;
    }
    LOG(TRACE) << "Error Handler function registred";
    return true;
}
