#include "frustrumholder.h"

using namespace parasolid::frustrum;

Holder::Holder()
{

}

Holder::~Holder()
{

}

Holder::MainFrustrum Holder::mainF() const
{
    return m_mainF;
}

void Holder::setMainF(const MainFrustrum &mainF)
{
    m_mainF = mainF;
}

Holder::DeltaFrustrum Holder::deltaF() const
{
    return m_deltaF;
}

void Holder::setDeltaF(const DeltaFrustrum &deltaF)
{
    m_deltaF = deltaF;
}

Holder::ErrorFrustrum Holder::errorF() const
{
    return m_errorF;
}

void Holder::setErrorF(const ErrorFrustrum &errorF)
{
    m_errorF = errorF;
}

Holder::GoFrustrum Holder::goF() const
{
    return m_goF;
}

void Holder::setGoF(const GoFrustrum &goF)
{
    m_goF = goF;
}
