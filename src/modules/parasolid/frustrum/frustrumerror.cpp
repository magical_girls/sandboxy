#include <logger/logger.h>

#include "frustrumfunctions.h"

namespace parasolid {
namespace frustrum
{

PK_ERROR_code_t errorHandler(PK_ERROR_sf_t *error)
{
    LOG(WARNING) << "Error while preforming " << error->function << ". Error code: "
        << error->code_token << ".";
    return error->code;
}

}
}
