#ifndef FRUSTRUMFUNCTIONS_H
#define FRUSTRUMFUNCTIONS_H

#include <parasolid_kernel.h>

namespace parasolid {
namespace frustrum {

extern "C" {

// main frustrum functions
void startFrustrum(int *);
void stopFrustrum(int *);
void getMemory(int *, char **, int *);
void returnMemory(int *, char **, int *);
void startFileFrustrum(int *);
void abortFrustrum(int *);
void stopFileFrustrum(int *);
void openReadFrustrumFile(const int *, const int *, const char *, const int *, const int *, int *, int *);
void openWriteFrustrumFile(const int *, const int *, const char *,const int *, const char *, const int *, int *, int *);
void readFromFrustrumFile(const int *, const int *, const int *, char *, int *, int *);
void writeToFrustrumFile(const int *, const int *, const int *, const char *, int *);
void closeFrustrumFile(const int *, const int *, const int *, int *);

// delta frustrum functions
PK_ERROR_code_t FRU_delta_open_for_write(PK_PMARK_t, PK_DELTA_t *);
PK_ERROR_code_t FRU_delta_open_for_read(PK_DELTA_t);
PK_ERROR_code_t FRU_delta_write(PK_DELTA_t, unsigned, const char *);
PK_ERROR_code_t FRU_delta_read(PK_DELTA_t, unsigned, char *);
PK_ERROR_code_t FRU_delta_delete(PK_DELTA_t);
PK_ERROR_code_t FRU_delta_close(PK_DELTA_t);
PK_ERROR_code_t FRU_delta_init(int);

// go frustrum functions
void openSegment(const int *, const int *, const int *, const int *, const double *, const int *, const int *, int *);
void closeSegment(const int *, const int *, const int *, const int *, const double *, const int *, const int *, int *);
void outputSegment(const int *, const int *, const int *, const int *, const double *, const int *, const int *, int *);

//error frustrum functions
PK_ERROR_code_t errorHandler(PK_ERROR_sf_t *);
}
}}

#endif // FRUSTRUMFUNCTIONS_H
