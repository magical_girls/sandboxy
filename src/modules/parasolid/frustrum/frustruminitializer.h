#ifndef FRUSTRUMINITIALIZER_H
#define FRUSTRUMINITIALIZER_H

#include "frustrumholder.h"

namespace parasolid {
namespace frustrum {

class Initializer
{
public:
    Initializer(Holder holder);
    void initHolder();
    bool registerFrustrum();

private:
    Holder m_holder;
};

}}


#endif // FRUSTRUMINITIALIZER_H
