#include <logger/logger.h>

#include <parasolid_kernel.h>
#include <frustrum_tokens.h>
#include <frustrum_ifails.h>

#include "frustrumfunctions.h"

namespace parasolid {
namespace frustrum
{

void parseFacet(const double *positions, const double *normals);
void parseLine(int segtyp, int ntags, const int *tags, int ngeoms, const double *geoms, int nlntp, const int *lntp);
void addCircle(const double *centre, const double *normal, double radius, const double *start, const double *end);
void addEllipse(const double *centre,
                const double *majorAxis,
                const double *minorAxis,
                double majorRadius,
                double minorRadius,
                const double *start,
                const double *end);

inline double VectorDot(const double *x, const double *y)
{
    return x[0] * y[0] + x[1] * y[1] + x[2] * y[2];
}

inline double *VectorSubtract(const double *x, const double *y, double *z)
{
    z[0] = x[0] - y[0];
    z[1] = x[1] - y[1];
    z[2] = x[2] - y[2];
    return z;
}

inline double *VectorAdd(const double *x, const double *y, double *z)
{
    z[0] = x[0] + y[0];
    z[1] = x[1] + y[1];
    z[2] = x[2] + y[2];
    return z;
}

inline double *VectorMult(const double *x, double m, double *y)
{
    y[0] = x[0] * m;
    y[1] = x[1] * m;
    y[2] = x[2] * m;
    return y;
}

inline double *VectorNormalise(double *x)
{
    return VectorMult(x, 1.0 / sqrt(VectorDot(x, x)), x);
}

inline double *VectorCross(const double *x, const double *y, double *z)
{
    z[0] = x[1] * y[2] - x[2] * y[1];
    z[1] = x[2] * y[0] - x[0] * y[2];
    z[2] = x[0] * y[1] - x[1] * y[0];
    return z;
}

void openSegment(const int *segtyp,
                 const int *ntags,
                 const int *tags,
                 const int *ngeoms,
                 const double *geoms,
                 const int *nlntp,
                 const int *lntp,
                 int *ifail)
{
    VLOG(8) << "Segment opening";
    //here we can specify color for the body/geom/etc
    switch (*segtyp) {
        case SGTPBY:
            VLOG(8) << "This is body with id " << tags[0];
            break;
        case SGTPFA:
            VLOG(8) << "This is face with id " << tags[0];
            break;
        case SGTPGC:
        case SGTPGS:
        case SGTPGB:
            VLOG(8) << "This is orphan geom with id " << tags[0];
            break;
        default:
            break;
    }
    *ifail = CONTIN;
}

void closeSegment(const int *segtyp,
                  const int *ntags,
                  const int *tags,
                  const int *ngeoms,
                  const double *geoms,
                  const int *nlntp,
                  const int *lntp,
                  int *ifail)
{
    VLOG(8) << "Segment closed";
    *ifail = CONTIN;
}

void outputSegment(const int *segtyp,
                   const int *ntags,
                   const int *tags,
                   const int *ngeoms,
                   const double *geoms,
                   const int *nlntp,
                   const int *lntp,
                   int *ifail)
{
    VLOG(8) << "Segment outputting " << tags[0];
    //we can cpecify color for highlighted elements
    switch (*segtyp) {
        case SGTPED: // edge
        case SGTPSI: // silhouette
        case SGTPPH: // planar hatch line
        case SGTPRH: // radial hatch line
        case SGTPRU: // rib line
        case SGTPBB: // blend boundary
        case SGTPPL: // parametric hatch line
            VLOG(8) << "This is lines probably";
            parseLine(*segtyp, *ntags, tags, *ngeoms, geoms, *nlntp, lntp);
            break;
        case SGTPGC: // curve (geometry)
        case SGTPGS: // surface (geometry)
        case SGTPGB: // surface boundary (geometry)
            VLOG(8) << "This is geometry probably";
            parseLine(*segtyp, *ntags, tags, *ngeoms, geoms, *nlntp, lntp);
            break;
        case SGTPMF: // mangled facet (output as triangular facet)
        case SGTPFT: // facet
            VLOG(8) << "This is facet probably";
            parseFacet(geoms, geoms + (*ngeoms / 2) * 3);
            break;
        default:
            break;
    }
    *ifail = CONTIN;
}

void parseFacet(const double *positions, const double *normals)
{
    for (int i = 0; i < 3; i++) {
//        GODataCollector::addVertexes(positions[i*3 + 0]);
//        GODataCollector::addVertexes(positions[i*3 + 1]);
//        GODataCollector::addVertexes(positions[i*3 + 2]);
        VLOG(9) << "Poligon [" << positions[i * 3 + 0] << "; " << positions[i * 3 + 1] << "; " << positions[i * 3 + 2]
                << "]";
//        GODataCollector::addNormals(normals[i*3 + 0]);
//        GODataCollector::addNormals(normals[i*3 + 1]);
//        GODataCollector::addNormals(normals[i*3 + 2]);
        VLOG(9) << "Normal [" << normals[i * 3 + 0] << "; " << normals[i * 3 + 1] << "; " << normals[i * 3 + 2] << "]";
    }
}

void parseLine(int segtyp, int ntags, const int *tags, int ngeoms, const double *geoms, int nlntp, const int *lntp)
{
    //QVector<double> data;
    switch (lntp[1]) {
        case L3TPSL:  // straight line
            for (int i = 0; i < 6; i++) {
                //data.push_back(geoms[i]);
            }
            //VLOG(9) << "Straight line data " << data;
            //GODataCollector::addEdges(qMove(data));
            break;
        case L3TPPY:  // poly-line
            for (int i = 0; i < ngeoms; i++) {
//            data.push_back(geoms[i * 3 + 0]);
//            data.push_back(geoms[i * 3 + 1]);
//            data.push_back(geoms[i * 3 + 2]);
            }
//        VLOG(9) << "Poly-line data " << data;
            //GODataCollector::addEdges(qMove(data));
            break;
        case L3TPCC:  // complete circle
            addCircle(geoms, geoms + 3, geoms[6], NULL, NULL);
            break;
        case L3TPCI:  // circular arc
            addCircle(geoms, geoms + 3, geoms[6], geoms + 7, geoms + 10);
            break;
        case L3TPCE:  // complete ellipse
            addEllipse(geoms, geoms + 3, geoms + 6, geoms[9], geoms[10], NULL, NULL);
            break;
        case L3TPEL:  // elliptical arc
            addEllipse(geoms, geoms + 3, geoms + 6, geoms[9], geoms[10], geoms + 11, geoms + 14);
            break;
        case L3TPPC:  // non-rational B-curve (Bezier)
        case L3TPRC:  // rational B-curve (Bezier)
        case L3TPNC:  // non-rational B-curve (NURBS)
        case L3TPRN:  // rational B-curve (NURBS)
            break;
        default:
            break;
    }
}

void addEllipse(const double *centre,
                const double *majorAxis,
                const double *minorAxis,
                double majorRadius,
                double minorRadius,
                const double *start,
                const double *end)
{
    const double twopi = 3.141592654 * 2.0;

    // maximum number of line segments in ellipse is defined below

    const int maxSegs = 40;

    // First de-parameterise the start & end point to get the start & end parameters
    // Then x = majorAxis * majorRadius * cos( x ) + minoraxis * minorRadius * sin( x )

    double param1, param2;

    if (start == NULL || end == NULL) {
        param1 = 0.0;
        param2 = twopi;
    }
    else {
        double vec[3];
        VectorSubtract(start, centre, vec);
        double cosval = VectorDot(vec, majorAxis) / majorRadius;

        if (fabs(cosval - 1.0) < 1.0e-8) {
            cosval = 1.0;
        }
        if (fabs(cosval + 1.0) < 1.0e-8) {
            cosval = -1.0;
        }
        param1 = acos(cosval);

        if (VectorDot(vec, minorAxis) < 0.0) {
            param1 = twopi - param1;
        }

        VectorSubtract(end, centre, vec);
        cosval = VectorDot(vec, majorAxis) / majorRadius;

        if (fabs(cosval - 1.0) < 1.0e-8) {
            cosval = 1.0;
        }
        if (fabs(cosval + 1.0) < 1.0e-8) {
            cosval = -1.0;
        }

        param2 = acos(cosval);

        if (VectorDot(vec, minorAxis) < 0.0) {
            param2 = twopi - param2;
        }
        if (param2 < param1) {
            param2 += twopi;
        }
    }

    // Work out no. of segs that we want.

    int nSegs = int(floor((param2 - param1) / twopi * maxSegs));
    nSegs = std::max(nSegs, 1);
    double majComp[3];
    double minComp[3];
    double loc[3];

//    QVector<double> data;
    for (int i = 0; i <= nSegs; i++) {
        double t = param1 + ((param2 - param1) * i) / nSegs;
        VectorMult(majorAxis, majorRadius * cos(t), majComp);
        VectorMult(minorAxis, minorRadius * sin(t), minComp);
        VectorAdd(centre, majComp, loc);
        VectorAdd(loc, minComp, loc);
//        data.push_back(loc[ 0 ]);
//        data.push_back(loc[ 1 ]);
//        data.push_back(loc[ 2 ]);
    }
    //GODataCollector::addEdges(qMove(data));
}

void addCircle(const double *centre, const double *normal, double radius, const double *start, const double *end)
{
    double majAxis[3];
    double minAxis[3];

    if (start == NULL || end == NULL) {
        PK_VECTOR1_t vec, vec2, vec3;
        for (int i = 0; i < 3; i++) {
            vec.coord[i] = normal[i];
            vec2.coord[i] = 0.0;
        }
        VectorNormalise(vec.coord);  // PK_VECTOR_perpendicular is very fussy about this.
        //PK_VECTOR_perpendicular( vec, vec2, &vec3 );
        for (int i = 0; i < 3; i++) {
            majAxis[i] = vec3.coord[i];
        }
    }
    else {
        VectorSubtract(start, centre, majAxis);
        VectorNormalise(majAxis);
    }
    VectorCross(normal, majAxis, minAxis);
    addEllipse(centre, majAxis, minAxis, radius, radius, start, end);
}

}
}
