import qbs 1.0

Project {
    name: "modules"

    references: [
        "parasolid/parasolid.qbs",
        "gui/gui.qbs"
    ]
}
