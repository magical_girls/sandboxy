#include "application.h"
#include <logger/logger.h>
#include <glew/GL/glew.h>
#include <GLFW/glfw3.h>
#include <imgui/imgui.h>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include "imgui_impl_glfw.h"

SHARE_EASYLOGGINGPP(getEasyloggingStorage())

static void error_callback(int error, const char* description)
{
    LOG(ERROR) << "Error " << error << ": " << description;
}

using namespace gui;

Application::Application()
{
    glfwSetErrorCallback(error_callback);
    if (!glfwInit()) {
        LOG(FATAL) << "Failed to init glfw!";
    }
    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    clear_color = ImColor(45, 45, 45);
    window = new MainWindow(glfwCreateWindow(1500, 1000, "Sandboxy", NULL, NULL));
    glfwMakeContextCurrent(*window);

    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    if (err != GLEW_OK) {
        LOG(FATAL) << "Failed to init glew! Error: " << glewGetErrorString(err);
    } else {
        LOG(DEBUG) << "Started OpenGL " << glGetString(GL_VERSION) << "; GLSL " << glGetString(GL_SHADING_LANGUAGE_VERSION);
    }
}

Application::Application(std::shared_ptr<api::IRenderer> renderer, std::shared_ptr<api::IBuilder> builder) :
    Application()
{
    window->setBuider(builder);
    window->setRenderer(renderer);
}

Application::~Application()
{
    ImGui_ImplGlfwGL3_Shutdown();
    glfwTerminate();
    delete window;
}

void Application::init()
{
    namespace fs = boost::filesystem;
    ImGui_ImplGlfwGL3_Init(*window, true);
    ImGuiIO& io = ImGui::GetIO();
    fs::path font_path(fs::initial_path<fs::path>() / fs::path("assets") / fs::path("fonts") / fs::path("FiraSans-Regular.ttf"));
    LOG(TRACE) << "Loading font from " << font_path;
    io.Fonts->AddFontFromFileTTF(font_path.string().c_str(), 14.0f, NULL, io.Fonts->GetGlyphRangesCyrillic());
}

void Application::runEventLoop()
{
    while (!glfwWindowShouldClose(*window) && !window->isClosed)
    {
        glfwPollEvents();
        ImGui_ImplGlfwGL3_NewFrame();

        window->showMainMenu();
        window->showElements();

        glViewport(0, 0, (int)ImGui::GetIO().DisplaySize.x, (int)ImGui::GetIO().DisplaySize.y);
        glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui::Render();
        glfwSwapBuffers(*window);
    }
}

