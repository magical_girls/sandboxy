#ifndef GUIAPPLICATION_H
#define GUIAPPLICATION_H

#include <memory>
#include <imgui/imgui.h>
#include <api/ibuilder.h>
#include <api/irenderer.h>
#include "mainwindow.h"

namespace gui {

class Application
{
public:
    Application();
    Application(std::shared_ptr<api::IRenderer> renderer, std::shared_ptr<api::IBuilder> builder);
    ~Application();
    void init();
    void runEventLoop();

private:
    MainWindow *window;
    ImVec4 clear_color;
};
} // namespace gui

#endif // GUIAPPLICATION_H
