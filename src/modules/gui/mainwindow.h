#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <api/ibuilder.h>
#include <api/irenderer.h>
#include "nodedmodeleditor.h"

struct GLFWwindow;

namespace gui {

class MainWindow
{
public:
    MainWindow(GLFWwindow *m_window);
    void setBuider(std::shared_ptr<api::IBuilder> builder);
    void setRenderer(std::shared_ptr<api::IRenderer> renderer);
    void showMainMenu();
    void showElements();

    operator GLFWwindow*();
    bool isClosed;

private:

private:
    std::shared_ptr<api::IBuilder> m_builder;
    std::shared_ptr<api::IRenderer> m_renderer;
    GLFWwindow *m_window;
    NodedModelEditor modelEditor;
    bool m_showEditor = false;
    bool m_showAppMetrics = false;
    bool m_showTestWindow = true;
    bool m_showNodedModelEditor = true;
    void testGui();
};
} // namespace gui

#endif // MAINWINDOW_H
