import qbs 1.0
import SandboxyModule

SandboxyModule {
    name: "gui"

    files: [
        "application.cpp",
        "application.h",
        "imgui_impl_glfw.cpp",
        "imgui_impl_glfw.h",
        "mainwindow.cpp",
        "mainwindow.h",
        "nodedmodeleditor.cpp",
        "nodedmodeleditor.h",
    ]

    property stringList gui_libs: ["boost_system", "boost_filesystem"]

    Properties {
        condition: qbs.targetOS.contains("windows")

        property string glfw_home: qbs.getEnv("GLFW_HOME")
        property string glm_home: qbs.getEnv("GLM_HOME")
        property string boost_home: qbs.getEnv("BOOST_HOME")

        cpp.includePaths: [
            glfw_home + "/include",
            glm_home + "/include",
            boost_home + "/include"
        ]

        cpp.libraryPaths: [
            glfw_home + "/lib",
            boost_home + "/lib"
        ]

        cpp.dynamicLibraries: gui_libs.concat([
            "glfw3",
            "opengl32",
            "winmm",
            "gdi32"
        ])
    }

    Properties {
        condition: qbs.targetOS.contains("linux")

        cpp.dynamicLibraries: gui_libs.concat([
            "glfw",
            "GL"
        ])
    }

    Depends { name: "api" }
    Depends { name: "glew" }
    Depends { name: "imgui" }
    Depends { name: "parasolid" }
}
