#include "mainwindow.h"
#include <GLFW/glfw3.h>
#include <imgui/imgui.h>
#include <api/ibuilder.h>

using namespace gui;

MainWindow::MainWindow(GLFWwindow *window) :
    isClosed(false)
{
    this->m_window = window;
}

void MainWindow::setBuider(std::shared_ptr<api::IBuilder> builder)
{
    m_builder = builder;
    modelEditor.setBuilder(builder);
}

void MainWindow::setRenderer(std::shared_ptr<api::IRenderer> renderer)
{
    m_renderer = renderer;
}

void MainWindow::showMainMenu()
{
    if (ImGui::BeginMainMenuBar()) {
        if (ImGui::BeginMenu("Main")) {
            ImGui::MenuItem("File");
            ImGui::Separator();
            ImGui::MenuItem("Exit");
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Edit")) {
            ImGui::MenuItem("Show editor", nullptr, &m_showNodedModelEditor);
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Debug")) {
            ImGui::MenuItem("Show metric", nullptr, &m_showAppMetrics);
            ImGui::EndMenu();
        }
        ImGui::EndMainMenuBar();
    }
}

void MainWindow::testGui()
{
    if (ImGui::Button("Build cube")) {
        m_builder.get()->buildDemo();
    }
}

void MainWindow::showElements()
{
    if (m_showAppMetrics) ImGui::ShowMetricsWindow(&m_showAppMetrics);
    if (m_showNodedModelEditor) modelEditor.showEditor(&m_showNodedModelEditor);
    if (m_showTestWindow) ImGui::ShowTestWindow(&m_showTestWindow);
    testGui();
}

MainWindow::operator GLFWwindow *()
{
    return m_window;
}

