#ifndef NODEDMODELEDITOR_H
#define NODEDMODELEDITOR_H

#include <iostream>
#include <vector>
#include <imgui/imgui.h>
#include <api/builder/node.h>
#include <api/ibuilder.h>
#include <boost/uuid/uuid.hpp>


namespace gui {

namespace buildElements {
//TODO: separate this!

class drawable_node {
private:
    api::node_base m_node;
public:
    ImVec2 pos;
    ImVec2 size;
public:
    drawable_node(api::node_base node) : m_node(node) {}
    drawable_node(const drawable_node& node) = default;
    api::node_base& node() { return m_node; }
};

} // namespace builElements

class NodedModelEditor
{
public:
    NodedModelEditor();
    void setBuilder(std::shared_ptr<api::IBuilder> builder);
    void showEditor(bool *opened);

private:
    void drawContextMenu(ImVec2 offset);
    void rendedPins(api::node_slot &slot, ImVec2 cursor_pos, ImVec2 slot_center_point, ImDrawList* draw_list);

private:
    std::vector<buildElements::drawable_node> m_nodes;
    std::shared_ptr<api::IBuilder> m_builder;
    boost::uuids::uuid selected_node_id;
    boost::uuids::uuid hovered_node_id;
    bool show_grid = false;
    bool slot_pressed = false;
    ImVec2 pressed_slot_pos;
    ImVec2 pressed_slot_nodepos;
    ImVec2 pressed_slot_offset;
    boost::uuids::uuid pressed_slot_id;
    bool pressed_slot_is_input;
    bool ready_for_link_creation = true;
    ImVec2 offset;
    ImVec2 scrolling = ImVec2(0.0f, 0.0f);
    const int SLOT_OFFSET = 30;
    const ImVec2 DEFAULT_NODE_SIZE = ImVec2(250, 50);
    const float NODE_SLOT_RADIUS = 4.0f;
    const int SLOT_SPACING = 40;
    const ImVec2 NODE_WINDOW_PADDING = ImVec2(8.0f, 8.0f);
};
} // namespace gui

#endif // NODEDMODELEDITOR_H
