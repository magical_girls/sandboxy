#include "nodedmodeleditor.h"
#include <imgui/imgui.h>
#include <imgui/imgui_internal.h>
#include <logger/logger.h>
#include <boost/uuid/uuid_io.hpp>
#include <future>
#include <chrono>

using namespace gui;
using namespace std;
using namespace boost;

#define DRAW_LOG false

static inline ImVec2 operator+(const ImVec2& lhs, const ImVec2& rhs) { return ImVec2(lhs.x + rhs.x, lhs.y + rhs.y); }
static inline ImVec2 operator-(const ImVec2& lhs, const ImVec2& rhs) { return ImVec2(lhs.x - rhs.x, lhs.y - rhs.y); }

NodedModelEditor::NodedModelEditor()
{

}

void NodedModelEditor::setBuilder(shared_ptr<api::IBuilder> builder)
{
    m_builder = builder;
}

void NodedModelEditor::showEditor(bool* opened)
{
    ImGui::SetNextWindowSize(ImVec2(700, 600), ImGuiSetCond_FirstUseEver);
    if (!ImGui::Begin("Model editor", opened)) {
        ImGui::End();
        return;
    }

    if (!ImGui::IsMouseDown(0)) {
        slot_pressed = false;
    }

    // Draw a list of nodes on the left side
    if (DRAW_LOG) LOG(TRACE) << "Drawing list of nodes on the left side";
    bool open_context_menu = false;
    ImGui::BeginChild("node_list", ImVec2(100, 0));
    ImGui::Text("Nodes");
    ImGui::Separator();

    for (auto &dnode : m_nodes) {
        ImGui::PushID(uuids::to_string(dnode.node().getId()).c_str());
        if (ImGui::Selectable((dnode.node().getNodePrefix() + uuids::to_string(dnode.node().getId())).c_str(),
                              dnode.node().getId() == selected_node_id)) {
            selected_node_id = dnode.node().getId();
        }
        if (ImGui::IsItemHovered()) {
            hovered_node_id = dnode.node().getId();
            open_context_menu |= ImGui::IsMouseClicked(1);
        }
        ImGui::PopID();
    }
    ImGui::EndChild();

    ImGui::SameLine();
    ImGui::BeginGroup();

    // Create our child canvas
    ImGui::Text("Hold middle mouse button to scroll (%.2f,%.2f)", scrolling.x, scrolling.y);
    ImGui::SameLine(ImGui::GetWindowWidth() - 100);
    ImGui::Checkbox("Show grid", &show_grid);
    ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(1, 1));
    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0, 0));
    ImGui::PushStyleColor(ImGuiCol_ChildWindowBg, ImColor(60, 60, 70, 200));
    ImGui::BeginChild("scrolling_region", ImVec2(0, 0), true, ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoMove);
    ImGui::PushItemWidth(120.0f);

    offset = ImGui::GetCursorScreenPos() - scrolling;
    ImDrawList* draw_list = ImGui::GetWindowDrawList();
    draw_list->ChannelsSplit(4);
    draw_list->ChannelsSetCurrent(0);
    // Display grid
    if (DRAW_LOG) LOG(TRACE) << "Drawing grid";
    if (show_grid) {
        const ImU32 grid_color = ImColor(200, 200, 200, 40);
        const float grid_size = 64.0f;
        ImVec2 win_pos = ImGui::GetCursorScreenPos();
        ImVec2 canvas_sz = ImGui::GetWindowSize();
        for (float x = fmodf(offset.x, grid_size); x < canvas_sz.x; x += grid_size)
            draw_list->AddLine(ImVec2(x, 0.0f) + win_pos, ImVec2(x, canvas_sz.y) + win_pos, grid_color);
        for (float y = fmodf(offset.y, grid_size); y < canvas_sz.y; y += grid_size)
            draw_list->AddLine(ImVec2(0.0f, y) + win_pos, ImVec2(canvas_sz.x, y) + win_pos, grid_color);
    }

    // Display links
    if (DRAW_LOG) LOG(TRACE) << "Drawing links";
    draw_list->ChannelsSetCurrent(1);
//    for (const buildElements::NodeLink &link : nodeLinks) {
//        ImGui::PushID(link.id);
//        //draw
//        ImGui::PopID();
//    }

    // Display nodes
    if (DRAW_LOG) LOG(TRACE) << "Drawing nodes";
    draw_list->ChannelsSetCurrent(3);
    for (auto &dnode : m_nodes) {
        ImGui::PushID(uuids::to_string(dnode.node().getId()).c_str());

        // calc node size
        dnode.size.x = DEFAULT_NODE_SIZE.x;
        dnode.size.y = DEFAULT_NODE_SIZE.y;
        float input_slots_heigth = (SLOT_OFFSET * 2) + ((dnode.node().getInputSlots().size() - 1) * SLOT_SPACING);
        float output_slots_heigth = (SLOT_OFFSET * 2) + ((dnode.node().getOutputSlots().size() - 1) * SLOT_SPACING);
        if ( input_slots_heigth > DEFAULT_NODE_SIZE.x || output_slots_heigth > DEFAULT_NODE_SIZE.y) {
            dnode.size.y = input_slots_heigth >= output_slots_heigth ? input_slots_heigth : output_slots_heigth;
        }

        // Display node contents first
        ImVec2 node_top_left = offset + dnode.pos;
        ImGui::SetCursorScreenPos(node_top_left + NODE_WINDOW_PADDING);
        ImGui::BeginGroup(); // Lock horizontal position
        ImGui::Text("%s", (dnode.node().getNodePrefix() + uuids::to_string(dnode.node().getId())).c_str());
        ImGui::EndGroup();

        // Display node box
        draw_list->ChannelsSetCurrent(2); // Background
        ImGui::SetCursorScreenPos(node_top_left);
        ImVec2 node_bottom_right = node_top_left + dnode.size;
        ImGui::InvisibleButton("node", dnode.size);

        bool is_node_hovered = false;
        if (ImGui::IsItemHovered()) {
            is_node_hovered = true;
        }

        bool node_moving_active = ImGui::IsItemActive();
        if (/*node_widgets_active || */node_moving_active) {
            //node_selected = node->ID;
        }

        if (node_moving_active && ImGui::IsMouseDragging(0)) {
            dnode.pos = dnode.pos + ImGui::GetIO().MouseDelta;
        }

        ImU32 node_bg_color = is_node_hovered || selected_node_id == dnode.node().getId() ?
                    ImColor(75,75,75) : ImColor(60,60,60);
        draw_list->AddRectFilled(node_top_left, node_bottom_right, node_bg_color, 4.0f);
        draw_list->AddRect(node_top_left, node_bottom_right, ImColor(100,100,100), 4.0f);

        //draw pins
        if (DRAW_LOG) LOG(TRACE) << "Drawing pins";

        draw_list->ChannelsSetCurrent(3);
        auto node_top_right  = ImVec2(node_bottom_right.x, node_top_left.y);

        int currentSlotNumber = 0;
        if (DRAW_LOG) LOG(TRACE) << "Drawing input pins";
        //input
        for (api::node_slot &slot : dnode.node().getInputSlots()) {
            ImGui::PushID(uuids::to_string(slot.id()).c_str());
            ImVec2 slot_center_point = ImVec2(node_top_left.x, node_top_left.y + SLOT_OFFSET + (SLOT_SPACING * currentSlotNumber++));
            auto old_cursor_pos = ImGui::GetCursorPos();
            ImVec2 cursor_screen_pos = slot_center_point - ImVec2(NODE_SLOT_RADIUS, 0);
            rendedPins(slot, cursor_screen_pos, slot_center_point, draw_list);
            ImGui::SetCursorPos(old_cursor_pos);
            ImGui::PopID();
        }

        //outputs are same
        if (DRAW_LOG) LOG(TRACE) << "Drawing outputs pins";
        currentSlotNumber = 0;
        for (api::node_slot slot : dnode.node().getOutputSlots()) {
            ImGui::PushID(uuids::to_string(slot.id()).c_str());
            ImVec2 slot_center_point = ImVec2(node_top_right.x, node_top_right.y + SLOT_OFFSET + (SLOT_SPACING * currentSlotNumber++));
            auto old_cursor_pos = ImGui::GetCursorPos();
            ImVec2 cursor_screen_pos = slot_center_point - ImVec2(NODE_SLOT_RADIUS, 0);
            rendedPins(slot, cursor_screen_pos, slot_center_point, draw_list);
            ImGui::SetCursorPos(old_cursor_pos);
            ImGui::PopID();
        }

        ImGui::PopID();
    }

    //draw "in progress" link
    if (DRAW_LOG) LOG(TRACE) << "Drawing wip link";
    if (ImGui::IsMouseDown(0) && slot_pressed) {
//        draw_list->AddBezierCurve(pressed_slot_pos + pressed_slot_offset, pressed_slot_pos + pressed_slot_offset + ImVec2(50, 0),
//                                  ImGui::GetMousePos() + ImVec2(-50, 0), ImGui::GetMousePos(),
//                                  ImColor(200, 200, 100), 3.0f);
        draw_list->AddLine(pressed_slot_pos, ImGui::GetMousePos(), ImColor(200, 200, 100), 3.0f);
    }
    draw_list->ChannelsMerge();

    drawContextMenu(offset);

    // Scrolling
    if (ImGui::IsWindowHovered() && !ImGui::IsAnyItemActive() && ImGui::IsMouseDragging(2, 0.0f))
        scrolling = scrolling - ImGui::GetIO().MouseDelta;

    ImGui::PopItemWidth();
    ImGui::EndChild();
    ImGui::PopStyleColor();
    ImGui::PopStyleVar(2);
    ImGui::EndGroup();

    ImGui::End();
}

void NodedModelEditor::drawContextMenu(ImVec2 offset)
{
    bool open_context_menu = false;
    if (!ImGui::IsAnyItemHovered() && ImGui::IsMouseHoveringWindow() && ImGui::IsMouseClicked(1)) {
        open_context_menu = true;
    }
    if (open_context_menu) {
        ImGui::OpenPopup("context_menu");
    }

    // Draw context menu
    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(8,8));
    if (ImGui::BeginPopup("context_menu"))
    {
        ImVec2 scene_pos = ImGui::GetMousePosOnOpeningCurrentPopup() - offset;
        if (ImGui::MenuItem("Add object")) {
            LOG(TRACE) << "Invoke parasolid builder for new obj node";
            api::node_base node = m_builder.get()->createObjectNode();
            auto dnode = buildElements::drawable_node(node);
            dnode.pos = scene_pos;
            m_nodes.push_back(dnode);
            LOG(TRACE) << "Node added";
        }
        if (ImGui::MenuItem("Paste", NULL, false, false)) {

        }
        ImGui::EndPopup();
    }
    ImGui::PopStyleVar();
}

void NodedModelEditor::rendedPins(api::node_slot &slot, ImVec2 cursor_pos, ImVec2 slot_center_point, ImDrawList *draw_list)
{
    ImGui::SetCursorScreenPos(cursor_pos);
    ImGui::InvisibleButton(uuids::to_string(slot.id()).c_str(), ImVec2(NODE_SLOT_RADIUS * 2, NODE_SLOT_RADIUS * 2));
    ImColor slot_color = ImColor(255, 255, 100);
    if (ImGui::IsItemHoveredRect()) {
        slot_color = ImColor(255, 100, 100);
        if (ImGui::IsMouseDown(0)) {
            if (ready_for_link_creation) {
                if (slot_pressed && slot.id() != pressed_slot_id) {
                    //release link
                    LOG(INFO) << "LINK CREATED!";
                    slot_pressed = false;
                    ready_for_link_creation = false;
                    async(launch::async, [this]() {
                        using namespace chrono_literals;
                        this_thread::sleep_for(500ms);
                        ready_for_link_creation = true;
                    });
                } else {
                    // link creatring started
                    pressed_slot_id = slot.id();
                    slot_pressed = true;
                    pressed_slot_pos = slot_center_point;
                }
            }
        }
    }
    draw_list->AddCircleFilled(slot_center_point, NODE_SLOT_RADIUS, slot_color);
}
