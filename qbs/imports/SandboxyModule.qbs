import qbs 1.0

SandboxyProduct {
    type: "dynamiclibrary"
    install_dir: project.base_path

    Depends { name: "logger" }

    Export {
        Depends { name: "cpp" }
        cpp.includePaths: [ includes ]
    }
}
