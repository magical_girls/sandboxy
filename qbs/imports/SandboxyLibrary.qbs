import qbs 1.0

SandboxyProduct {
    type: "dynamiclibrary"
    install_dir: project.library_path

    Depends {
        condition: project.tests_enabled
        name: "Qt.test"
    }

    Export {
        Depends { name: "cpp" }
        cpp.includePaths: [ includes ]
    }
}
