import qbs 1.0

Product {
    property string install_dir
    property string includes: ".."

    Depends { name: "cpp" }

    cpp.cxxLanguageVersion: "c++14"
    cpp.defines: project.defines.concat("__cplusplus=201402L") //bugfix

    Group {
        fileTagsFilter: product.type

        qbs.install: true
        qbs.installDir: install_dir
    }
}
